package com.malex.exception;

/** Todo sut up status code */
public class TemplateResolverException extends RuntimeException {

  public TemplateResolverException(Throwable cause) {
    super(cause);
  }
}
