package com.malex.model.request;

public record MessageRequest(Long chatId, String text) {}
