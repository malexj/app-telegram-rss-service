package com.malex.model.request;

public record ImageSearchRequest(String url, String imageId) {}
